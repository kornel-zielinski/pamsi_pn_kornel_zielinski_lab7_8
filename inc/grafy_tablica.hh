#ifndef GRAFY_TABLICA
#define GRAFY_LISTA

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cmath>
#include <sys/time.h>
#include <vector>
#include "grafy.hh"

namespace tab
{
  
  class Vert: public base::Vert
  {
  public:
    int key;
    Vert(std::string name) : base::Vert(name) {key=-1;}
  };

  class Graph: public base::Graph
  {
  public:
    //std::vector<Vert*> _vert;
    int _size;
    base::Edge*** _inc;//idence
    void insert_vertex(std::string name);
    void insert_edge(int, std::string, std::string);
    void insert_edge(int, Vert*, Vert*);
    bool are_adjacent(std::string, std::string);
    bool are_adjacent(Vert*, Vert*);
    Vert* opposite(Vert*, base::Edge*);
    Graph(int size);
    ~Graph();
    //void print();
  };

}

#endif
