#ifndef GRAFY_LISTA
#define GRAFY_LISTA

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cmath>
#include <sys/time.h>
#include <vector>
#include <list>
#include <fstream>
#include "grafy.hh"

namespace list
{
  class Vert: public base::Vert
  {
  public:
    std::vector<base::Edge*> inc;//idence
    Vert(std::string name) : base::Vert(name) {}
  };
  
  class Graph: public base::Graph
  {
  public:
    //int _size;
    void insert_vertex(std::string name);
    void insert_edge(int, std::string, std::string);
    void insert_edge(int, Vert*, Vert*);
    bool are_adjacent(std::string, std::string);
    bool are_adjacent(Vert*, Vert*);
    //Vert* opposite(Vert*, base::Edge*);
    //Graph(int size);
    ~Graph();
    //void print();
    void save(std::string);
    void load(std::string);
    void kruskal(Graph&);
  };  
}

#endif
