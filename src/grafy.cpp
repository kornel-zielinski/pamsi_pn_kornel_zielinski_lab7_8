#include "grafy.hh"
using namespace base;

Edge::Edge(int value, Vert* vert1, Vert* vert2)
{
    _value=value;
    _vert[0]=vert1;
    _vert[1]=vert2;
}

void Graph::insert_vertex(std::string vert_name)
{
  Vert* temp;
  for(int i=0; i<_vert.size(); i++)
  {
    if(vert_name.compare(_vert[i]->name())==0)
      return;
  }
  temp = new Vert(vert_name);
  _vert.push_back(temp);
}

void Graph::insert_edge(int value, Vert* vert1, Vert* vert2)
{
  Edge* temp;
  for(int i=0; i<_edge.size(); i++)
  {
    if(vert1 == _edge[i]->_vert[0] || vert1 == _edge[i]->_vert[1])
    {
      if(vert2 == _edge[i]->_vert[0] || vert2 == _edge[i]->_vert[1])
        return;
    }
  }
  temp = new Edge(value, vert1, vert2);
  _edge.push_back(temp);
}

void Graph::insert_edge(int value, std::string name1, std::string name2)
{   
    Vert *vert1 = 0, *vert2 = 0;
    Edge* temp;

    if(!name1.compare(name2))
        return;
    
    insert_vertex(name1);
    insert_vertex(name2);

    for(int i=0; i<_vert.size() && (vert1 == 0 || vert2 == 0); i++)
    {
        if(name1.compare(_vert[i]->name())==0)
            vert1 = _vert[i];
        else if(name2.compare(_vert[i]->name())==0)
            vert2 = _vert[i];
    }
    
    insert_edge(value, vert1, vert2);
}

void Graph::print()
{
    std::cout << "Vertices (" << _vert.size() << "):\n";
    for(int i=0; i<_vert.size(); i++)
        std::cout << _vert[i]->name() << '\n';
    std::cout << "Edges: (" << _edge.size() << "):\n";
    for(int i=0; i<_edge.size(); i++)
    {
        std::cout << _edge[i]->_vert[0]->name() << ", " 
                  << _edge[i]->_vert[1]->name() << ", " 
                  << _edge[i]->_value << '\n';
    }
}

