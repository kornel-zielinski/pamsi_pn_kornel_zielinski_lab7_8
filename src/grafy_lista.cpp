#include "grafy_lista.hh"

using namespace list;

Graph::~Graph()
{
  int size = _vert.size();
  for(int i=0; i<size; i++)
    delete _vert[i];
  _vert.clear();
  size = _edge.size();
  for(int i=0; i<size; i++)
    delete _edge[i];
  _edge.clear();
}

void Graph::insert_vertex(std::string name)
{
  Vert* temp;
  int size = _vert.size();
  for(int i=0; i<size; i++)
  {
    if(name.compare(_vert[i]->name())==0)
      return;
  }
  temp = new Vert(name);
  _vert.push_back(temp);
}


void Graph::insert_edge(int value, Vert* vert1, Vert* vert2)
{
  base::Edge* temp;
  int size = _edge.size();
  
  for(int i=0; i<size; i++)
  {
    if(vert1 == _edge[i]->_vert[0] || vert1 == _edge[i]->_vert[1])
    {
      if(vert2 == _edge[i]->_vert[0] || vert2 == _edge[i]->_vert[1])
        return;
    }
  }
  
  temp = new base::Edge(value, vert1, vert2);
  _edge.push_back(temp);
  vert1->inc.push_back(temp);
  vert2->inc.push_back(temp);
}

void Graph::insert_edge(int value, std::string name1, std::string name2)
{   
    Vert *vert1 = 0, *vert2 = 0;

    if(!name1.compare(name2))
        return;
    
    insert_vertex(name1);
    insert_vertex(name2);

    for(int i=0; i<_vert.size() && (vert1 == 0 || vert2 == 0); i++)
    {
        if(name1.compare(_vert[i]->name())==0)
	  vert1 = (Vert*) _vert[i];
        else if(name2.compare(_vert[i]->name())==0)
	  vert2 = (Vert*) _vert[i];
    }
    insert_edge(value, vert1, vert2);
}

bool Graph::are_adjacent(std::string vert1_name, std::string vert2_name)
{
  Vert* vert1=0, * vert2=0;
  if(!vert1_name.compare(vert2_name))
    return true;
  
  for(int i=0; i<_vert.size() && (vert1 == 0 || vert2 == 0); i++)
    {
        if(vert1_name.compare(_vert[i]->name())==0)
	  vert1 = (Vert*) _vert[i];
        else if(vert2_name.compare(_vert[i]->name())==0)
	  vert2 = (Vert*) _vert[i];
    }
  if(vert1==0 || vert2==0)
    return false;

  return are_adjacent(vert1, vert2);
}
bool Graph::are_adjacent(Vert* vert1, Vert* vert2)
{
  int size;
  if(vert1==0 || vert2 == 0 || vert1 == vert2)
    return false;
  size = vert1->inc.size();
  for(int i=0; i < size; i++)
    {
      if(vert1->inc[i]->_vert[0] == vert2 || vert1->inc[i]->_vert[1] == vert2)
	return true;
    }
  return false;
}


void Graph::save(std::string filename)
{
  std::ofstream out;
  int size = _edge.size();
  
  out.open(filename.c_str());
  for(int i=0; i < size; i++)
  {
    out << _edge[i]->_vert[0]->name() << ", " 
        << _edge[i]->_vert[1]->name() << ", " 
        << _edge[i]->_value << '\n';
  }
}

void Graph::load(std::string filename)
{
  std::ifstream in;
  std::string buffer, vert1, vert2, val;
  int pos;
  
  in.open(filename.c_str());

  while(in.good())
  {
    std::getline(in, buffer);
    if(in.good())
    {
      vert1 = buffer.substr(0, pos = buffer.find(","));
      buffer = buffer.substr(pos+2);
      vert2 = buffer.substr(0, pos = buffer.find(","));
      val = buffer.substr(pos+2);
      insert_edge(atoi(val.c_str()), vert1, vert2);
    }
    
  }
}

void Graph::kruskal(Graph& tree)
{
  std::list<base::Edge*> edges;
  std::list<base::Edge*>::iterator iter = edges.begin();
  base::Edge* temp;
  bool vert1_som = false, vert2_som = false;
  
  for(int i=0; i < _edge.size(); i++)
  {
    temp = _edge[i];
    iter = edges.begin();
    while(iter != edges.end())
    {
      if((*iter)->value() >= temp->value())
	break;
      iter++;
    }
    edges.insert(iter, temp);
  }
  
  while(!edges.empty())
  {
    temp = edges.front();
    edges.pop_front();
    
    for(int i=0; i<tree._vert.size() && !(vert1_som && vert2_som); i++)
    {
      if(!temp->_vert[0]->_name.compare(tree._vert[i]->_name))
	vert1_som = true;
      if(!temp->_vert[1]->_name.compare(tree._vert[i]->_name))
	vert2_som = true;
    }
    
    if(!(vert1_som && vert2_som))
    {
      if(!vert1_som)
	tree.insert_vertex(temp->_vert[0]->_name);
      if(!vert2_som)
	tree.insert_vertex(temp->_vert[1]->_name);
      tree.insert_edge(temp->_value, (Vert*)temp->_vert[0], (Vert*)temp->_vert[1]);
    }
  }
}
