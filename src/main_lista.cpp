#include "grafy_lista.hh"

int main()
{
  list::Graph ayy, lmao;
  ayy.insert_vertex("ABC");
  ayy.insert_vertex("DEF");
  
  ayy.insert_edge(10, "ABC", "DEF");
  ayy.insert_edge(20, "ABC", "GHI" );
  ayy.insert_edge(10, "GHI", "DEF");
  
  if(ayy.are_adjacent("ABC", "DEF"))
    std::cout << "ABC DEF som\n";
  if(ayy.are_adjacent("GHI", "DEF"))
    std::cout << "GHI DEF som\n";
  ayy.print();
  /*ayy.save("fug.txt");
  lmao.load("fug.txt");
  lmao.print();*/
  ayy.kruskal(lmao);
  std::cout << '\n';
  lmao.print();
  return 0;
}
