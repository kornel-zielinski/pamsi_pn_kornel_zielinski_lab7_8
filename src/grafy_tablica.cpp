#include "grafy_tablica.hh"

using namespace tab;

Graph::Graph(int size)
{
  _size=size;
  _inc = new base::Edge**[size];
  for(int i=0; i<size; i++)
  {
    _inc[i] = new base::Edge*[size];
    for(int j=0; j<size; j++)
      _inc[i][j]=0;
  }  
}

Graph::~Graph()
{
  int size = _vert.size();
  for(int i=0; i<_size; i++)
    delete _inc[i];
  delete _inc;

  for(int i=0; i<size; i++)
    delete _vert[i];
    _vert.clear();

  size = _edge.size();
  for(int i=0; i<size; i++)
    delete _edge[i];
    _edge.clear();
}

void Graph::insert_vertex(std::string vert_name)
{
  Vert* temp;
  for(int i=0; i<_vert.size(); i++)
  {
    if(vert_name.compare(_vert[i]->name())==0)
      return;
  }
  temp = new Vert(vert_name);
  if(_vert.size()<_size)
  {
    temp->key = _vert.size();
    _vert.push_back(temp);
  }
}

void Graph::insert_edge(int value, Vert* vert1, Vert* vert2)
{
  base::Edge* temp; 
  for(int i=0; i<_edge.size(); i++)
  {
    if(vert1 == _edge[i]->_vert[0] || vert1 == _edge[i]->_vert[1])
    {
      if(vert2 == _edge[i]->_vert[0] || vert2 == _edge[i]->_vert[1])
        return;
    }
  }
  temp = new base::Edge(value, vert1, vert2);
  _inc[vert1->key][vert2->key] = temp;
  _inc[vert2->key][vert1->key] = temp;
  _edge.push_back(temp);
}

void Graph::insert_edge(int value, std::string name1, std::string name2)
{   
    Vert *vert1 = 0, *vert2 = 0;
    //base::Edge* temp;

    if(!name1.compare(name2))
        return;
    
    insert_vertex(name1);
    insert_vertex(name2);

    for(int i=0; i<_vert.size() && (vert1 == 0 || vert2 == 0); i++)
    {
        if(name1.compare(_vert[i]->name())==0)
	  vert1 = (Vert*) _vert[i];
        else if(name2.compare(_vert[i]->name())==0)
	  vert2 = (Vert*) _vert[i];
    }
    insert_edge(value, vert1, vert2);
}

/*
void Graph::print()
{
  int size = _vert.size();
  std::cout << "Vertices (" << size << "):\n";
  for(int i=0; i<size; i++)
     std::cout << _vert[i]->name() << '\n';
  size = _edge.size();
  std::cout << "Edges: (" << size << "):\n";
  for(int i=0; i < size; i++)
  {
    std::cout << _edge[i]->_vert[0]->name() << ", " 
              << _edge[i]->_vert[1]->name() << ", " 
              << _edge[i]->_value << '\n';
  }
}
*/

bool Graph::are_adjacent(std::string vert1_name, std::string vert2_name)
{
  Vert* vert1=0, * vert2=0;
  if(!vert1_name.compare(vert2_name))
    return true;
  
  for(int i=0; i<_vert.size() && (vert1 == 0 || vert2 == 0); i++)
    {
        if(vert1_name.compare(_vert[i]->name())==0)
	  vert1 = (Vert*) _vert[i];
        else if(vert2_name.compare(_vert[i]->name())==0)
	  vert2 = (Vert*) _vert[i];
    }
  if(vert1==0 || vert2==0)
    return false;

  return are_adjacent(vert1, vert2);
      
}

bool Graph::are_adjacent(Vert* vert1, Vert* vert2)
{
  if(vert1==0 || vert2 == 0)
    return false;
  if(_inc[vert1->key][vert2->key]==0)
    return false;
  else
    return true;
}

Vert* Graph::opposite(Vert* vert, base::Edge* edge)
{
  if(vert == edge->_vert[0])
    return (Vert*)edge->_vert[1];
  if(vert == edge->_vert[1])
    return (Vert*)edge->_vert[0];
  return 0;
}
